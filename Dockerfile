FROM openjdk:11
ARG JAR_FILE=target/*.jar
COPY . /tmp_build
WORKDIR /tmp_build
RUN ./mvnw -T 1C clean install -Dmaven.test.skip -DskipTests -Dmaven.javadoc.skip=true \ 
&& cp ${JAR_FILE} /app.jar \
&& rm -rf *
EXPOSE 8080
ENTRYPOINT ["java,"-jar","/src/app.jar"]

#FROM adoptopenjdk/openjdk11
#FROM openjdk:11 
#ARG JAR_FILE=target/*.jar
#COPY ${JAR_FILE} app.jar
#ENTRYPOINT ["java","-jar","/app.jar"]